﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace MegaManga
{
    public partial class Viewer : Window
    {
        string exeDirectory = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString();
        BitmapImage[] images;
        private int currentIndex, length;
        private float ratioX, ratioY, imageWidth, imageHeight, canvasWidth, canvasHeight;

        //properties
        public string MangaName { get; set; }
        public string ChapterName { get; set; }

        private void MangaImage_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            PreviousImage();
        }

        private void MangaImage_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            NextImage();
        }

        public Viewer()
        {
            InitializeComponent();
            currentIndex = 0;
            //initially hide image row
            HideImageRow();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DisposeImages();
        }


        //--------------------Image Functions--------------------

        public void ShowImageRow()
        {
            LoadingRow.Height = new GridLength(0);
            ImageRow.Height = new GridLength(1, GridUnitType.Star);
        }

        public void HideImageRow()
        {
            ImageRow.Height = new GridLength(0);
            LoadingRow.Height = new GridLength(1, GridUnitType.Star);
        }

        public void NextImage()
        {
            if(currentIndex < length - 1)
            {
                currentIndex++;
                MangaImage.Source = images[currentIndex];
            }
            else
            {
                currentIndex = 0;
                MangaImage.Source = images[currentIndex];
            }
            UpdateTitle();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(e.PreviousSize != new Size(0,0))
            {
                FitImage();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void MangaImage_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            NextImage();
        }

        private void MangaImage_PreviewMouseRightButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            PreviousImage();
        }

        public void PreviousImage()
        {
            if (currentIndex > 0)
            {
                currentIndex--;
                MangaImage.Source = images[currentIndex];
            }
            else
            {
                currentIndex = length-1;
                MangaImage.Source = images[currentIndex];
            }
            UpdateTitle();
        }

        public void FitImage()
        {
            //get the heights and ratios
            try
            {
                canvasHeight = (float)MainGrid.ActualHeight - (float)MainMenu.ActualHeight;
                canvasWidth = (float)MainGrid.ActualWidth;
                imageWidth = (float)images[currentIndex].PixelWidth;
                imageHeight = (float)images[currentIndex].PixelHeight;
                ratioY = imageHeight / imageWidth;
                ratioX = imageWidth / imageHeight;
            }
            catch
            {
                MessageBox.Show("cannot get image height");
            }
            //determine which way to scale to edges
            if (canvasHeight / imageHeight < canvasWidth / imageWidth)
            {
                MangaImage.Height = canvasHeight;
                MangaImage.Width = ratioX * canvasHeight;
                CenterImage();
            }
            else if(canvasHeight / imageHeight > canvasWidth / imageWidth)
            {
                MangaImage.Width = canvasWidth;
                MangaImage.Height = ratioY * canvasWidth;
                CenterImage();
            }
        }

        public void CenterImage()
        {
            UpdateTitle();
            Canvas.SetTop(MangaImage, (canvasHeight / 2) - (MangaImage.Height / 2));
            Canvas.SetLeft(MangaImage, (canvasWidth / 2) - (MangaImage.Width / 2));
        }

        public void UpdateTitle()
        {
            this.Title = MangaName + " -- " + ChapterName + " -- [" + (currentIndex+1) + "/" + length + "]";
        }

        public void DisposeImages()
        {
            MangaImage.Source = null;
            
            for(int i=0; i<images.Length; i++)
            {
                images[i].UriSource = null;
                images[i] = null;
            }
        }

        public void LoadImages(int numImages, string dir)
        {
            length = numImages;
            images = new BitmapImage[length];
            for(int i=0; i<length; i++)
            {
                images[i] = new BitmapImage();
                var stream = File.OpenRead(dir + i + ".jpg");
                images[i].BeginInit();
                images[i].CacheOption = BitmapCacheOption.OnLoad;
                images[i].StreamSource = stream;
                images[i].EndInit();
                stream.Close();
                stream.Dispose();
            }
            //set the initial image
            ShowImageRow();
            MangaImage.Source = images[currentIndex];
            UpdateTitle();
            FitImage();
        }
        //-------------------------------------------------------

        
    }
}
