﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Net;
using HtmlAgilityPack;
using System.IO;

namespace MegaManga
{
    public class MangaSearchEngine
    {
        //current manga variables
        public Manga currentManga = new Manga();

        string exeDirectory = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString();
        HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
        HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
        WebClient webClient = new WebClient();
        HtmlNode[] nodes;
        MangaSearchResult[] mangaSearchResults;
        ChapterSearchResult[] chapterSearchResults;
        public string url;
        public int imageCount;

        public MangaSearchEngine()
        {

        }

        public MangaSearchResult[] SearchManga(string searchTerm)
        {
            try
            {
                doc = web.Load("http://manganel.com/search/" + searchTerm);
                nodes = doc.DocumentNode.SelectNodes("//div[contains(@class, 'daily-update-item')]").ToArray();
            }
            catch
            {
                MessageBox.Show("No manga found");
                return null;
            }

            //only search through and assign nodes if it is not null
            if (nodes != null)
            {
                mangaSearchResults = new MangaSearchResult[nodes.Length];
                HtmlNode nameNode, chapterNode;

                //add an entry to the search list for each manga series found
                for (int i = 0; i < nodes.Length; i++)
                {
                    //get the name and chapter html nodes
                    nameNode = nodes[i].SelectSingleNode(".//span//a");
                    chapterNode = nodes[i].SelectSingleNode(".//a[contains(@class, 'item-chapter')]");
                    url = nameNode.Attributes["href"].Value.ToString();
                    mangaSearchResults[i] = new MangaSearchResult(nameNode.InnerHtml.ToString(), chapterNode.InnerHtml.ToString(), url);
                }
                return mangaSearchResults;
            }
            else
            {
                //MessageBox.Show("No manga found.");
                return null;
            }
        }


        public ChapterSearchResult[] SearchChapters(string url)
        {
            try
            {
                doc = web.Load(url);
                nodes = doc.DocumentNode.SelectNodes("//div[contains(@class, 'row')]//span//a").ToArray();
            }
            catch
            {
                MessageBox.Show("No Chapters");
                return null;
            }

            //only search through and assign nodes if it is not null
            if (nodes != null)
            {
                chapterSearchResults = new ChapterSearchResult[nodes.Length];
                HtmlNode nameNode;

                //add an entry to the search list for each chapter found
                for (int i = 0; i < nodes.Length; i++)
                {
                    //get the chapter html nodes
                    nameNode = nodes[i];
                    url = nameNode.Attributes["href"].Value.ToString();
                    chapterSearchResults[i] = new ChapterSearchResult(nameNode.InnerHtml.ToString(), url);
                }
                return chapterSearchResults;
            }
            else
            {
                //MessageBox.Show("No manga found.");
                return null;
            }
            //returns an array with chapter name preview image url, and synopsis
        }


        public async Task SearchImages(string url)
        {
            string[] imageURLS;
            //create required directories
            string tempDir = exeDirectory + @"\temp\";
            Directory.CreateDirectory(tempDir);
            //create a viewer object and pass the images to it
            Viewer viewer = new Viewer();
            viewer.Visibility = Visibility.Visible;
            viewer.MangaName = currentManga.MangaName;
            viewer.ChapterName = currentManga.ChapterName;
            //try to download the images
            try
            {
                imageCount = 0;
                doc = web.Load(url);
                nodes = doc.DocumentNode.SelectNodes("//div[contains(@class, 'vung-doc')]//img").ToArray();
                imageCount = nodes.Length;
                imageURLS = new string[nodes.Length];

                //download the images
                for (int i = 0; i < nodes.Length; i++)
                {
                    //viewer.SetLoadingText(i + 1, nodes.Length);
                    imageURLS[i] = nodes[i].Attributes["src"].Value.ToString();
                    await webClient.DownloadFileTaskAsync(new Uri(imageURLS[i]), tempDir + i + ".jpg");
                }
            }
            catch
            {
                MessageBox.Show("Images failed to load");
            }
            viewer.LoadImages(nodes.Length, tempDir);
        }
    }

    //------search result classes------
    public class MangaSearchResult
    {
        public string mangaName, currentChapter, url;
        public MangaSearchResult(string n, string c, string u)
        {
            mangaName = n;
            currentChapter = c;
            url = u;
        }
    }

    public class ChapterSearchResult
    {
        public string chapterName, url;
        public ChapterSearchResult(string c, string u)
        {
            chapterName = c;
            url = u;
        }
    }
}
