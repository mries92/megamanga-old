﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaManga
{
    public class Manga
    {
        public string MangaName { get; set; }
        public string MangaSynopsis { get; set; }
        public string ChapterUrl { get; set; }
        public string ChapterName { get; set; }
        public int MangaLength { get; set; }
        public int chapterLength { get; set; }


        public Manga()
        {

        }
    }
}
