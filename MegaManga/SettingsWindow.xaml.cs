﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace MegaManga
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public MainWindow MainWindow { get; }

        public SettingsWindow(MainWindow mainWindow)
        {
            this.MainWindow = mainWindow;
            InitializeComponent();
            Row_GeneralSettings.Height = new GridLength(1, GridUnitType.Star);
        }

        public void ApplySettings()
        {

        }

        private void Button_GeneralSettings_Click(object sender, RoutedEventArgs e)
        {
            //hide other settings grids
            Row_GeneralSettings.Height = new GridLength(1, GridUnitType.Star);
        }

        private void Button_ColorSettings_Click(object sender, RoutedEventArgs e)
        {
            //hide other settings grids
            Row_GeneralSettings.Height = new GridLength(0);
        }

        private void Button_Apply_Click(object sender, RoutedEventArgs e)
        {
            ApplySettings();
            MainWindow.RefreshUi();
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            ApplySettings();
            MainWindow.RefreshUi();
            Close();
        }
    }
}
