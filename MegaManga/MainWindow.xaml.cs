﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;

namespace MegaManga
{
    public partial class MainWindow : Window
    {
        SettingsWindow settingsWindow;
        ObservableCollection<mResult> mResults = new ObservableCollection<mResult>();
        ObservableCollection<cResult> cResults = new ObservableCollection<cResult>();
        //hold the results of each search
        private MangaSearchResult[] mangaResults;
        private ChapterSearchResult[] chapterResults;
        private MangaSearchEngine searchEngine = new MangaSearchEngine();
        private string searchString;
        //ui variables
        private int MARGIN;
        

        public MainWindow()
        {
            InitializeComponent();
            //initialize from settings
            LoadSettings();
            //formatting
            MARGIN = 3;
            ChapterResultsList.ItemsSource = cResults;
            MangaResultsList.ItemsSource = mResults;
            RefreshUi();
        }

        private void SearchBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (SearchBox.Text == "Search Manga")
            {
                SearchBox.Text = "";
            }
        }

        private void SearchManga()
        {
            searchString = SearchBox.Text;
            searchString = searchString.Replace(" ", "_");
            mangaResults = searchEngine.SearchManga(searchString);
            HideChapterResults();
            mResults.Clear();
            try
            {
                for (int i = 0; i < mangaResults.Length; i++)
                {
                    mResults.Add(new mResult { MangaName = mangaResults[i].mangaName, CurrentChapter = mangaResults[i].currentChapter });
                }
            }
            catch
            {

            }
            SearchBox.Text = "";
            //scroll back to top of list
            try
            {
                MangaResultsList.ScrollIntoView(MangaResultsList.Items[0]);
            }
            catch { }
        }

        private void SearchChapters()
        {
            chapterResults = searchEngine.SearchChapters(mangaResults[MangaResultsList.SelectedIndex].url);
            HideMangaResults();
            cResults.Clear();
            for (int i = 0; i < chapterResults.Length; i++)
            {
                cResults.Add(new cResult { ChapterName=chapterResults[i].chapterName });
            }
            ChapterResultsList.ScrollIntoView(ChapterResultsList.Items[0]);
        }

        private void HideMangaResults()
        {
            MangaResultsRow.Height = new GridLength(0);
            ChapterResultsRow.Height = new GridLength(1,GridUnitType.Star);
        }

        private void HideChapterResults()
        {
            ChapterResultsRow.Height = new GridLength(0);
            MangaResultsRow.Height = new GridLength(1, GridUnitType.Star);
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                SearchManga();
            }
        }

        private void MangaResultsList_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SearchChapters();
            searchEngine.currentManga.MangaName = mangaResults[MangaResultsList.SelectedIndex].mangaName;
            MangaInfoLabel.Content = "Current Manga: " + searchEngine.currentManga.MangaName;
        }

        private void TextSizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(e.OldValue != 0)
            {
                Properties.Settings.Default.TextSize = (int)TextSizeSlider.Value;
                Properties.Settings.Default.Save();
                MangaResultsList.FontSize = Properties.Settings.Default.TextSize;
                ChapterResultsList.FontSize = Properties.Settings.Default.TextSize;
            }
        }

        private async void ChapterResultsList_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            searchEngine.currentManga.ChapterName = chapterResults[ChapterResultsList.SelectedIndex].chapterName;
            searchEngine.currentManga.ChapterUrl = chapterResults[ChapterResultsList.SelectedIndex].url;
            await searchEngine.SearchImages(chapterResults[ChapterResultsList.SelectedIndex].url);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void SearchBox_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            //set the placeholder text
            SearchBox.Text = "Search Manga";
        }

        public void RefreshUi()
        {
            MangaResultsList.Margin = new Thickness(MARGIN, MARGIN, MARGIN, MARGIN);
            ChapterResultsList.Margin = new Thickness(MARGIN, MARGIN, MARGIN, MARGIN);
            SearchButton.Margin = new Thickness(MARGIN, MARGIN, MARGIN, MARGIN);
            SearchBox.Margin = new Thickness(MARGIN, MARGIN, MARGIN, MARGIN);
            TextSizeSlider.Margin = new Thickness(MARGIN, MARGIN, MARGIN, MARGIN);
            MangaInfoLabel.Margin = new Thickness(MARGIN, MARGIN, MARGIN, MARGIN);

            //results list settings
            HideChapterResults();
            MangaResultsList.AutoGenerateColumns = false;
            ChapterResultsList.AutoGenerateColumns = false;
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //dont update on init
            if (e.PreviousSize != new Size(0, 0))
            {
                //dont update settings on maximize
                if(e.NewSize != new Size(SystemParameters.PrimaryScreenWidth + 16, SystemParameters.PrimaryScreenHeight + 16))
                {
                    Properties.Settings.Default.MainWindowSize = new System.Drawing.Size((int)e.NewSize.Width, (int)e.NewSize.Height);
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void LoadSettings()
        {
            TextSizeSlider.Value = Properties.Settings.Default.TextSize;
            this.Width = Properties.Settings.Default.MainWindowSize.Width;
            this.Height = Properties.Settings.Default.MainWindowSize.Height;
        }

        private void PreferencesMenuItem_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            settingsWindow = new SettingsWindow(this);
            settingsWindow.Visibility = Visibility.Visible;
        }
    }

    //data source for manga list
    struct mResult
    {
        public string MangaName { get; set; }
        public string CurrentChapter { get; set; }
    }
    //data source for chapter list
    struct cResult
    {
        public string ChapterName { get; set; }
    }
}
